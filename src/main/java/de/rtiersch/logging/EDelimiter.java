/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.logging;

import org.jetbrains.annotations.NotNull;

public enum EDelimiter
{
    /**
     * The delimiters for surrounding the keywords like Log.Level.*.toString().
     */
    LOG("[", "]"),

    /**
     * White WhoAmI.
     */
    FORMAT_WHOAMI("\u001B[1;37m", "\u001B[0m"),

    /**
     * White WhoAmI.
     */
    FORMAT_CLASS("\u001B[1;30m", "\u001B[0m"),

    /**
     * Magenta threadname.
     */
    FORMAT_THREAD("\u001B[1;35m", "\u001B[0m"),

    /**
     * Cyan time.
     */
    FORMAT_TIME("\u001B[1;36m", "\u001B[0m"),

    /**
     * Bold text. TODO 21m is not working!!!
     */
    FORMAT_BOLD("\u001B[1m", "\u001B[0m"),

    /**
     * Underlined text.
     */
    FORMAT_UNDERLINE("\u001B[4m", "\u001B[24m"),

    /**
     * Green info.
     */
    FORMAT_INF("\u001B[32m", "\u001B[39m"),

    /**
     * No Formation.
     */
    FORMAT_INF_TEXT("", "\u001B[39m"),

    /**
     * Yellow warning.
     */
    FORMAT_WAR("\u001B[33m", "\u001B[93m"),

    /**
     * No Formation.
     */
    FORMAT_WAR_TEXT("", "\u001B[39m"),

    /**
     * Red warning.
     */
    FORMAT_ERR("\u001B[31m", "\u001B[39m"),

    /**
     * No Formation.
     */
    FORMAT_ERR_TEXT("", "\u001B[93m"),

    /**
     * Blue warning.
     */
    FORMAT_DEB("\u001B[34m", "\u001B[93m"),

    /**
     * No Formation.
     */
    FORMAT_DEB_TEXT("", "\u001B[39m");

    /**
     * If surround should do anything.
     */
    public static boolean s_active = true;
    /**
     * Left delimiter.
     */
    private final String m_left;
    /**
     * Right delimiter.
     */
    private final String m_right;

    /**
     * Initializes the m_left and m_right delimiters.
     *
     * @param p_left  m_left delimiter.
     * @param p_right m_right delimiter.
     */
    EDelimiter(final String p_left, final String p_right)
    {
        this.m_left = p_left;
        this.m_right = p_right;
    }

    /**
     * Surrounds the String with the delimiters.
     *
     * @param p_string input.
     * @return Surrounded string.
     */
    public final @NotNull String surround(final String p_string)
    {
        if (s_active) {
            return this.m_left + p_string + this.m_right;
        } else {
            return p_string;
        }
    }

    /**
     * Surrounds the String with the delimiters.
     *
     * @param p_string input.
     * @return Surrounded string.
     */
    public final @NotNull String surroundF(final String p_string)
    {
        return this.m_left + p_string + this.m_right;
    }
}
