/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.logging;

import de.rtiersch.logging.help.LevelInstance;
import de.rtiersch.logging.help.TraceLock;
import de.rtiersch.logging.help.TupleNameLock;
import de.rtiersch.logging.instance.File;
import de.rtiersch.logging.instance.Syslog;
import de.rtiersch.logging.instance.Terminal;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import static de.rtiersch.logging.EDelimiter.FORMAT_BOLD;
import static de.rtiersch.logging.EDelimiter.FORMAT_CLASS;
import static de.rtiersch.logging.EDelimiter.FORMAT_THREAD;
import static de.rtiersch.logging.EDelimiter.FORMAT_TIME;
import static de.rtiersch.logging.EDelimiter.FORMAT_WHOAMI;
import static de.rtiersch.logging.EDelimiter.LOG;

/**
 * Created by Raphael on 02.10.2015. This is a singleton class to log messages
 * to the Terminal. It has five different levels available: NONE, ERROR, WARN,
 * INFO, DEBUG. WARN includes ERROR, INFO includes WARN (and ERROR) and DEBUG
 * will print everything. With NONE you will turn the logging off. If you change
 * the log-level it will print an warn-message to the Terminal if allowed to
 * (log-level dependent). TODO implement logging to syslog. --Nice to have TODO
 * implement getStackTrace parsing/logging with a method register(String m_name)
 * so classes are able to set a m_name. TODO Wrapper-Class für HashMap mit
 * Tuple
 */
public final class Log
{

    /**
     * The number of stacktrace entries that are scipped to get the name of
     * the caller Class if the class is accesed over Singleton.
     */
    private static final int              STACK_UP_INSTANCE = 3;
    /**
     * The number of stacktrace entries that are scipped to get the name of
     * the caller Class if the class is accesed statically.
     */
    private static final int              STACK_UP_STATIC   = 4;
    /**
     * The heart of the Singleton. This will initialize this class and remembers
     * the pointer.
     */
    private static final Log              OUR_INSTANCE      = new Log();
    /**
     *
     */
    private static final SimpleDateFormat S_TIME_FORMAT     =
        new SimpleDateFormat("HH:mm:ss.SSS");
    /**
     * TraceCache.
     */
    private final        TraceCache       m_cache           = new TraceCache();
    /**
     * The Name of this class for logging.
     */
    private final @NotNull String m_name;
    /**
     * The logging level for each individual type. It can be changed through
     * setting it with Log.inst().setLogLevel(Log.EType type, [Log.ELevel
     * m_logLevel]);
     */
    private final HashMap<EType, LevelInstance>               m_logLevel   =
        new HashMap<>();
    /**
     * m_trace.
     */
    private final ArrayList<TupleNameLock> m_trace      = new ArrayList<>();
    /**
     * Should color markers be added?
     */
    private       boolean                                     m_colourized = true;

    /**
     * Constructor. Initializes the final values.
     */
    private Log()
    {
        this.m_name = "LogInstance";

        this.m_logLevel.put(EType.TERMINAL, new LevelInstance(ELevel.NONE, new Terminal()));
        this.m_logLevel.put(EType.FILE, new LevelInstance(ELevel.NONE, new File()));
        this.m_logLevel.put(EType.SYSLOG, new LevelInstance(ELevel.NONE, new Syslog()));
    }

    /**
     * Same as getInstance() for better readability in the code.
     *
     * @return self return.
     */
    @NotNull
    public static Log inst()
    {
        return Log.getInstance();
    }

    /**
     * The standard method called to get the Instance of the singleton.
     *
     * @return self return.
     */
    @NotNull
    public static Log getInstance()
    {
        return Log.OUR_INSTANCE;
    }

    /**
     * Prints an info-level message.
     *
     * @param p_message    String The message to log.
     * @param p_identifier String Keyword / Topic of the message.
     * @return self return.
     */
    @NotNull
    public static Log Info(
        final @NotNull String p_message, final @NotNull String p_identifier)
    {
        return Log.inst()
                  .write(p_message, p_identifier, ELevel.INFO, STACK_UP_STATIC);
    }

    /**
     * Prints an info-level message.
     *
     * @param p_message String The message to log.
     * @return self return.
     */
    @NotNull
    public static Log Info(
        final @NotNull String p_message)
    {
        return Log.inst().write(p_message, null, ELevel.INFO, STACK_UP_STATIC);
    }

    /**
     * Prints an debug-level message.
     *
     * @param p_message    String The message to log.
     * @param p_identifier String Keyword / Topic of the message.
     * @return self return.
     */
    @NotNull
    public static Log Debug(
        final @NotNull String p_message, final @NotNull String p_identifier)
    {
        return Log.inst()
                  .write(p_message, p_identifier, ELevel.DEBUG,
                         STACK_UP_STATIC);
    }

    /**
     * Prints an debug-level message.
     *
     * @param p_message String The message to log.
     * @return self return.
     */
    @NotNull
    public static Log Debug(
        final @NotNull String p_message)
    {
        return Log.inst().write(p_message, null, ELevel.DEBUG, STACK_UP_STATIC);
    }

    /**
     * Prints an warn-level message.
     *
     * @param p_message    String The message to log.
     * @param p_identifier String Keyword / Topic of the message.
     * @return self return.
     */
    @NotNull
    public static Log Warn(
        final @NotNull String p_message, final @NotNull String p_identifier)
    {
        return Log.inst()
                  .write(p_message, p_identifier, ELevel.WARN, STACK_UP_STATIC);
    }

    /**
     * Prints an warn-level message.
     *
     * @param p_message String The message to log.
     * @return self return.
     */
    @NotNull
    public static Log Warn(
        final @NotNull String p_message)
    {
        return Log.inst().write(p_message, null, ELevel.WARN, STACK_UP_STATIC);
    }

    /**
     * Prints an error-level message.
     *
     * @param p_message    String The message to log.
     * @param p_identifier String Keyword / Topic of the message.
     * @return self return.
     */
    @NotNull
    public static Log Error(
        final @NotNull String p_message, final @NotNull String p_identifier)
    {
        return Log.inst()
                  .write(p_message, p_identifier, ELevel.ERROR,
                         STACK_UP_STATIC);
    }

    /**
     * Prints an error-level message.
     *
     * @param p_message String The message to log.
     * @return self return.
     */
    @NotNull
    public static Log Error(
        final @NotNull String p_message)
    {
        return Log.inst().write(p_message, null, ELevel.ERROR, STACK_UP_STATIC);
    }

    /**
     * Adds a new trace element to the log.
     *
     * @param p_name Name of the trace.
     * @return Lock for the trace.
     */
    public static Log.Lock traceAdd(@NotNull final String p_name)
    {
        return new Log.Lock(p_name, Log.OUR_INSTANCE);
    }

    /**
     * TODO ERROR handling. Tries to set an output. Will print an error if
     * Object is invalid.
     *
     * @param p_output new output. "File" need to get a String.
     * @return Self return.
     */
    public @NotNull Log setOutput(final Object p_output)
    {
        for (final EType l_type : EType.values()) {
            this.m_logLevel.get(l_type).getInstance().setOutput(p_output);
        }
        return this;
    }

    /**
     * Changes the level of logging for each type of logging. Default values
     * are: TERMINAL=INFO SYSLOG  =NONE FILE =NONE
     *
     * @param p_logType  Log.EType Type of logging which should be altered.
     * @param p_logLevel Log.ELevel Level of logging for the selected type.
     * @return self return.
     */
    public @NotNull Log setLogLevel(
        final @NotNull EType p_logType, final @NotNull ELevel p_logLevel)
    {
        this.m_logLevel.get(p_logType).setLevel(p_logLevel);
        this.warn("Log Level of " + p_logType.toString() + " Changed to " + p_logLevel
            .toString(), this.m_name);
        return this;
    }

    /**
     * Changes the level of logging for each type of logging. Default values
     * are: TERMINAL=INFO SYSLOG  =NONE FILE =NONE
     *
     * @param p_logType  Log.EType Type of logging which should be altered.
     * @param p_logLevel Log.ELevel Level of logging for the selected type.
     * @return self return.
     */
    public @NotNull Log setLogLevel(
        final @NotNull EType p_logType, final String p_logLevel)
    {
        ELevel l_level = ELevel.INFO;
        for (final ELevel l_eLevel : ELevel.values()) {
            if (l_eLevel.toString().equals(p_logLevel)) {
                l_level = l_eLevel;
                break;
            }
        }
        this.m_logLevel.get(p_logType).setLevel(l_level);
        this.warn("Log Level of " + p_logType.toString() + " Changed to "
                      + l_level.toString(), this.m_name);
        return this;
    }

    /**
     * Prints an info-level message.
     *
     * @param p_message    String The message to log.
     * @param p_identifier String Keyword / Topic of the message.
     * @return self return.
     *
     * @deprecated since 02.05.2017
     */
    @Deprecated
    public @NotNull Log info(
        final @NotNull String p_message, final String p_identifier)
    {
        return this.write(p_message, p_identifier, ELevel.INFO,
                          STACK_UP_INSTANCE);
    }

    /**
     * Prints an debug-level message.
     *
     * @param p_message    String The message to log.
     * @param p_identifier String Keyword / Topic of the message.
     * @return self return.
     *
     * @deprecated since 02.05.2017
     */
    @Deprecated
    public @NotNull Log debug(
        final @NotNull String p_message, final String p_identifier)
    {
        return this.write(p_message, p_identifier, ELevel.DEBUG,
                          STACK_UP_INSTANCE);
    }

    /**
     * Prints an warn-level message.
     *
     * @param p_message    String The message to log.
     * @param p_identifier String Keyword / Topic of the message.
     * @return self return.
     *
     * @deprecated since 02.05.2017
     */
    @Deprecated
    public @NotNull Log warn(
        final @NotNull String p_message, final String p_identifier)
    {
        return this.write(p_message, p_identifier, ELevel.WARN,
                          STACK_UP_INSTANCE);
    }

    /**
     * Prints an error-level message.
     *
     * @param p_message    String The message to log.
     * @param p_identifier String Keyword / Topic of the message.
     * @return self return.
     *
     * @deprecated since 02.05.2017
     */
    @Deprecated
    public @NotNull Log error(
        final @NotNull String p_message, final String p_identifier)
    {
        return this.write(p_message, p_identifier, ELevel.ERROR,
                          STACK_UP_INSTANCE);
    }

    /**
     * Prints the messages.
     *
     * @param p_message      String The message to log.
     * @param p_identifier   String Keyword / Topic of the message.
     * @param p_level        Log Level.
     * @param p_stackUpSteps How many steps do I need to go up in the stacktrace
     *                       to find the caller class?
     * @return self return.
     */
    private @NotNull Log write(
        final @NotNull String p_message,
        final @Nullable String p_identifier,
        final @NotNull ELevel p_level,
        final int p_stackUpSteps)
    {

        String l_formatStack = "";
        String l_formatIdentify = "";
        String l_formatThread = "";

        final int l_logLevelTerminal =
            this.m_logLevel.get(EType.TERMINAL).getLevel().getValue();
        if (l_logLevelTerminal >= ELevel.DEBUG.getValue()) {
            l_formatStack = FORMAT_CLASS.surround(LOG.surroundF(
                Thread.currentThread()
                      .getStackTrace()[p_stackUpSteps].getClassName()));
            l_formatThread = FORMAT_THREAD.surround(
                LOG.surroundF(Thread.currentThread().getName()));
        }
        if (p_identifier != null) {
            l_formatIdentify = LOG.surroundF(p_identifier);
        }


        final String l_logLevel = FORMAT_BOLD.surround(p_level.getNameFmt());
        final String l_trace;
        l_trace =
            FORMAT_WHOAMI.surround(this.generateTrace() + l_formatIdentify);
        final String l_message;
        l_message = p_level.getFormatText().surround(this.delLn(p_message));
        final String l_formatTime = FORMAT_TIME.surround(
            LOG.surroundF(S_TIME_FORMAT.format(new Date())));

        final String l_lM;
        l_lM =
            l_logLevel + l_formatTime + l_formatThread + l_trace + l_formatStack
                + " " + l_message;

        for (final EType l_type : EType.values()) {
            if (this.m_logLevel.get(l_type).getLevel().getValue()
                >= p_level.getValue())
            {
                final boolean l_success;
                l_success =
                    this.m_logLevel.get(l_type).getInstance().write(l_lM);

                if (!l_success) {
                    this.m_logLevel.get(l_type).setLevel(ELevel.NONE);
                    this.error("Could not write to " + l_type.toString() + ". This output is deactivated for further"
                                   + " messages", this.m_name);
                }
            }
        }
        return this;
    }

    /**
     * Deletes all NewLine and CarriageReturn characters.
     *
     * @param p_message String.
     * @return parsed String.
     */
    private String delLn(final @NotNull String p_message)
    {
        return p_message.replaceAll("\n|\r", "");
    }

    /**
     * Stops all logging instances (smooth stop).
     */
    public void stop()
    {
        for (final EType l_type : EType.values()) {
            this.m_logLevel.get(l_type).getInstance().stop();
        }
    }

    /**
     * Adds a new m_trace at the end of the tracelist. If String is null it will
     * be an not displayed anchor.
     *
     * @param p_name Name of the m_trace.
     * @param p_lock lock to remove this m_trace.
     * @return index of added element.
     *
     * @deprecated since 02.05.2017
     */
    @Deprecated
    public int traceAdd(final String p_name, final TraceLock p_lock)
    {
        this.m_trace.add(new TupleNameLock(p_name, p_lock));
        this.m_cache.m_validTrace = false;
        return this.m_trace.size() - 1;
    }

    /**
     * Removes this m_trace and all after it.
     *
     * @param p_index index of the m_trace.
     * @param p_lock  lock for the m_trace.
     * @return true if successfully.
     *
     * @deprecated since 02.05.2017
     */
    @Deprecated
    public boolean traceRm(final int p_index, final TraceLock p_lock)
    {
        if (this.traceRmAfter(p_index, p_lock)) {
            this.m_trace.remove(p_index);
            return true;
        }
        if (p_index >= this.m_trace.size()) {
            return false;
        }
        final TupleNameLock l_traceTuple = this.m_trace.get(p_index);
        if (l_traceTuple.m_lock == p_lock) {
            this.m_trace.remove(p_index);
            this.m_cache.m_validTrace = false;
            return true;
        }
        return false;
    }

    /**
     * Removes all the traces after this one.
     *
     * @param p_index index of the m_trace.
     * @param p_lock  lock for the m_trace.
     * @return true if successfully.
     *
     * @deprecated since 02.05.2017
     */
    @Deprecated
    public boolean traceRmAfter(final int p_index, final TraceLock p_lock)
    {
        if (p_index >= (this.m_trace.size() - 1)) {
            return false;
        }
        final TupleNameLock l_traceTuple = this.m_trace.get(p_index);
        if (l_traceTuple.m_lock == p_lock) {
            for (int l_i = this.m_trace.size() - 1; l_i > p_index; l_i--) {
                this.m_trace.remove(l_i);
            }
            this.m_cache.m_validTrace = false;
            return true;
        }
        return false;
    }

    /**
     * Generates the traceroute.
     *
     * @return traceroute.
     */
    private String generateTrace()
    {
        /*
         * Looks if the traceroute is still cached.
         */
        if (this.m_cache.m_validTrace) {
            return this.m_cache.m_cacheTrace;
        }
        /*
         * Generates new traceroute.
         */
        final int l_traceSize = this.m_trace.size();
        final int l_maxTrace = 10;
        String l_dots = "";
        final String[] l_elements = new String[l_maxTrace];
        Arrays.fill(l_elements, "");
        // Dot counter
        if (l_traceSize > l_maxTrace) {
            int l_sizeWithoutAnchor = l_traceSize;
            for (final TupleNameLock l_tupleNameLock : this.m_trace) {
                if (l_tupleNameLock.m_name == null) {
                    l_sizeWithoutAnchor--;
                }
            }
            if ((l_sizeWithoutAnchor - l_maxTrace) > 0) {
                l_dots =
                    StringUtils.repeat(".", l_sizeWithoutAnchor - l_maxTrace);
            }
        }
        int l_arraySavingPosition = l_maxTrace - 1;
        // iterate backwards through the list
        for (int l_i = l_traceSize - 1; l_i >= 0; l_i--) {
            if (l_arraySavingPosition < 0) {
                // reached maximum elements to print.
                break;
            } else {
                // Is this an Anchor?
                if (this.m_trace.get(l_i).m_name == null) {
                    // Yes
                    continue;
                } else {
                    // No. Save it.
                    l_elements[l_arraySavingPosition] =
                        LOG.surroundF(this.m_trace.get(l_i).m_name);
                    l_arraySavingPosition--;
                }
            }
        }
        /*
         * Saves it and returns.
         */
        this.m_cache.m_cacheTrace = l_dots + de.rtiersch.util.lists.StringUtils.toString(l_elements);
        this.m_cache.m_validTrace = true;
        return this.m_cache.m_cacheTrace;
    }

    /**
     * Is the output coloured?
     *
     * @return true if yes.
     */
    public boolean isColourized()
    {
        return this.m_colourized;
    }

    /**
     * Should the output be olourized?
     *
     * @param p_coloured true if yes.
     */
    public void setColourized(final boolean p_coloured)
    {
        this.m_colourized = p_coloured;
    }

    /**
     * Lock container for easier tracelock managenment.
     */
    public static class Lock
        extends TraceLock
    {
        /**
         * Index.
         */
        private final int m_index;
        /**
         * Logging instance to use.
         */
        private final Log m_log;

        /**
         * Creates a new Lock object.
         *
         * @param p_lockname name of the trace.
         * @param p_log      Logging instance to use.
         */
        private Lock(
            @NotNull final String p_lockname, @NotNull final Log p_log)
        {
            this.m_index = p_log.traceAdd(p_lockname, this);
            this.m_log = p_log;
        }

        /**
         * Creates a new Lock object.
         *
         * @param p_lockname name of the trace.
         */
        public Lock(
            @NotNull final String p_lockname)
        {
            this.m_index = Log.inst().traceAdd(p_lockname, this);
            this.m_log = Log.inst();
        }

        /**
         * Removes this trace from the log.
         */
        public void remove()
        {
            this.m_log.traceRm(this.m_index, this);
        }

        /**
         * Removes all trace elements after this one.
         */
        public void removeAfter()
        {
            this.m_log.traceRmAfter(this.m_index, this);
        }
    }

    /**
     * Lock container for easier tracelock managenment.
     */
    public static class CountingLock
        extends TraceLock
    {
        /**
         * Name.
         */
        private final String m_name;
        /**
         * Logging instance to use.
         */
        private final Log    m_log;
        /**
         * Index.
         */
        private       int    m_index;
        /**
         * Counts how many times this log was added and removed.
         */
        private       int    m_counter;

        /**
         * Creates a new Lock object.
         *
         * @param p_name name of the trace.
         * @param p_log  Logging instance to use.
         */
        public CountingLock(
            @NotNull final String p_name, @NotNull final Log p_log)
        {
            this.m_name = p_name;
            this.m_log = p_log;
        }

        /**
         * Creates a new Lock object.
         *
         * @param p_name name of the trace.
         */
        public CountingLock(@NotNull final String p_name)
        {
            this.m_log = Log.OUR_INSTANCE;
            this.m_name = p_name;
        }

        /**
         * Add this key to the log if not already done.
         */
        public void add()
        {
            if (this.m_counter == 0) {
                this.m_index = this.m_log.traceAdd(this.m_name, this);
            }
            this.m_counter++;
        }

        /**
         * Removes this trace from the log.
         * Only the first one who used add is allowed to use this method.
         */
        public void remove()
        {
            if (this.m_counter == 0) {
                return;
            }
            if (this.m_counter == 1) {
                this.m_log.traceRm(this.m_index, this);
            }
            this.m_counter--;
        }

        /**
         * Removes all trace elements after this one.
         * Only the first one who used add is allowed to use this method.
         */
        public void removeAfter()
        {
            if (this.m_counter == 0) {
                return;
            }
            if (this.m_counter == 1) {
                this.m_log.traceRmAfter(this.m_index, this);
            }
        }
    }
}
