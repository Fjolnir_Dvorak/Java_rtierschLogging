/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.logging.instance;

import org.jetbrains.annotations.Nullable;

/**
 * Created by Rafael on 02.10.2015.
 * TODO Refactor this class design. It is plain ugly.
 */
public interface IWriteToInstance {
    /**
     * Writes a new line into the output stream.
     *
     * @param p_input String without a new Line at the end.
     * @return true if write was valid.
     */
    boolean write(String p_input);

    /**
     * Starts the output stream.
     * @return self.
     */
    @Nullable IWriteToInstance start();

    /**
     * Stops the output stream.
     * @return self.
     */
    @Nullable IWriteToInstance stop();

    /**
     * Passes output information. Needed for File.
     * TODO ugly!
     * @param p_output Output object.
     * @return self.
     */
    @Nullable IWriteToInstance setOutput(Object p_output);
}
